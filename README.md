# NetworkOfRTemperatureSensors
### Description of the project
This is iot project were via the MQTT protocol (a raspberry pi in the role of the broker):

- we collect data from many sensors (esp8266 + )
- store it in a database located on the server end (SQLite + flask)
- display it on a web page (Chart.js) dynamically (every second)
- based on the average value from the sensors light up a different led

### Role of each file
- app.py: flask server that subscribes and receives messages from precise topics, stores and fetches from the DB, commands the LEDs and renders the template.
- main.html: web page and javascript code that asks for data every x seconds and using Chart.js draw the graph
- sensordata.db: database
- simulation.sh: if you don't have enough hardware you can simulate the esp+sensor via this bash script 
